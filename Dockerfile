FROM python:3.6-alpine3.7

LABEL maintainer="Ihor Kharkhun"

RUN mkdir /downloader

COPY ./requirements.txt /downloader/requirements.txt

WORKDIR /downloader

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev \
  && pip install -r requirements.txt --no-cache-dir \
  && apk del build-deps

COPY ./downloader.py /downloader/downloader.py
COPY ./db.py /downloader/db.py
COPY ./logger.py /downloader/logger.py

CMD ["python3", "downloader.py"]