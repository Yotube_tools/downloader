#!/usr/bin/env python3
import os
import pika
from db import Db
import boto3
import logger
from pytube import YouTube

db = Db()
base_video_url = 'https://www.youtube.com/watch?v='  # Standard video link


def pika_connecting():  # Connecting rabbitmq
    logger.info(f'''IP PIKA: {os.environ["PIKA_CONNECT_IP"]}''')
    connection = pika.BlockingConnection(pika.ConnectionParameters(host = os.environ['PIKA_CONNECT_IP'], port = '5672')) #Connection to Rabbit
    channel = connection.channel()
    channel.queue_declare(queue=os.environ['PIKA_QUEUE_NAME']) #Declare queue
    return connection

def link_generator(video_id): # The function generates a link to the video
    url = base_video_url + video_id
    return url

def downloader(url): # The function downloads a video
    YouTube(url).streams.first().download() # /streams.first()download hightest qualiti video/download(path)
    return YouTube(url).streams.first().default_filename

def upload_file(file_name, bucket_name): # Uploading video to bucket in S3
    s3.upload_file(file_name, bucket_name, file_name)
    return

def connect_to_s3(): # Connecting to Minio S3
    s3 = boto3.client('s3', endpoint_url=os.environ['S3_ENDPOINT_URL'], use_ssl=False,
                    aws_access_key_id=os.environ['S3_ACCESS_KEY'], aws_secret_access_key=os.environ['S3_SECRET_KEY'])
    return s3

def callback(channel, method, properties, body): # Start downloads task in queue
    channel.basic_ack(delivery_tag = method.delivery_tag)
    if db.check_downloading(str(body.decode('utf-8'))) == False:
        db.update_downloading(int(body.decode('utf-8')))
        url = link_generator(db.get_video_id(body.decode('utf-8')))
        file_name = downloader(url)        
        upload_file(file_name, 'videos')
        db.update_downloaded(str(body.decode('utf-8')))

if __name__ == "__main__":
    logger = logger.logger # Initialization logger
    logger.info('>>> >>> >>> DOWNLOADER <<< <<< <<<')
    s3 = connect_to_s3() # Connect to s3

    connection = pika_connecting() #Connection to Rabbit
    channel = connection.channel()
    channel.queue_declare(queue=os.environ['PIKA_QUEUE_NAME']) #Declare queue
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback, queue=os.environ['PIKA_QUEUE_NAME'])
    channel.start_consuming()
