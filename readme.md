EN:
Downloader is a microservice that downloads videos from Youtube by getting jobs from channel_parser by RabbitMQ. Downloader receives the task, downloads the video, saves the downloaded video to the S3 file storage, reports the completion and waits for a new job. The micro-service architecture allows you to run several Channel_parser to increase the number of simultaneously processed tasks.

1. The network should have a database with which to take information about the task. Connection parameters must be written to the environment variable DB_CONNECT_STR which is in the file env.list. Variable format: {DB_name}://{user}:{password}@{IP}:{Port}
2. To receive tasks for downloading on the network should be RabbitMQ. The IP address of RabbitMQ needs to be written to the environment variable PIKA_CONNECT_IP. The name of the queue in the variable PIKA_QUEUE_NAME. The variables are in the env.list file.
3. The network must have file storage. The variables S3_ENDPOINT_URL, S3_ACCESS_KEY, S3_SECRET_KEY, S3_BUCKET_NAME should be written to the env.list file.
	An example of running Minio in Docker: docker run -p 9000:9000 minio/minio server /data
4. You need to build a Docker container
	Example of the Docker command (run in the project folder): docker build -t downloader:1.0.0 .
5. Run the created container with the parameter --env-file where it specifies the path to the file with the environment variables.
	An example of a container run command: docker run --env-file ./env.list downloader:1.0.0
6. Downloader will automatically start processing tasks from the queue. To accelerate the processing, you can run more containers with a microService Downloader


RUS:
Downloader это микросервис который скачивает видео с Youtube получая задания от channel_parser по RabbitMQ. Downloader получает задачу, скачивает видео, сохраняет скачаное видео в файловое хранилище S3, сообщает о завершении и ожидает нового задания. Микросервисная архитектура позволяет запускать одновременно несколько Channel_parser для увеличения количества одновременно обрабатываемых задач.

1. В сети должна быть база данных с которой берется информация о поступившей задачи. Параметры подключения нужно записать в переменную среды DB_CONNECT_STR которая находится в файле env.list. Формат переменной: {DB_name}://{user}:{password}@{IP}:{Port}
2. Для получения задач на скачивание в сети должен быть RabbitMQ. IP адресс RabbitMQ нужно записать в переменную среды PIKA_CONNECT_IP. Название очереди в переменную PIKA_QUEUE_NAME. Переменные находятся в файле env.list.
3. В сети должно быть файловое хранилище. Переменные S3_ENDPOINT_URL, S3_ACCESS_KEY, S3_SECRET_KEY, S3_BUCKET_NAME записать в файл env.list
	Пример запуска Minio в Docker: docker run -p 9000:9000 minio/minio server /data
4. Нужно построить Docker контейнер
	Пример команды Docker(запускаем в папке с проэктом): docker build -t downloader:1.0.0 .
5. Запускаем созданый контейнер с параметром --env-file где указывает путь к файлу с переменными среды.
	Пример команды запуска контейнера: docker run --env-file ./env.list downloader:1.0.0
6. Downloader автоматически начнет обрабатывать задачи из очереди. Для ускорения обработки можно запустить больше контейнеров с микросервисом Downloader