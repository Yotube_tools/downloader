import logger
import os
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select, delete, update
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, Text, DateTime

Base = declarative_base()
logger = logger.logger
logger.info('>>> >>> >>> DB <<< <<< <<<')

class Video(Base):
    __tablename__ = 'videos'
    id = Column(Integer, primary_key=True)
    channel_id = Column(String(250))
    channel_title = Column(Text())
    video_id = Column(String(250))
    video_title = Column(Text())
    puplished_at = Column(DateTime())
    description = Column(Text())
    date_task = Column(DateTime())
    downloading = Column(Boolean())
    downloaded = Column(Boolean())
    path_to_video = Column(Text())

    def __str__(self):
        return f'> Video: id:{self.id} video_id: {self.video_id} title: {self.video_title}'


class Db():
    session = None
    engine = None

    def __init__(self):
        logger.info('>>> >>> >>> DB activated <<< <<< <<<')
        self.engine = self.get_engine()
        self.create_all(self.engine)
        self.session = self.get_session(self.engine)
        return

    def get_engine(self):
        logger.info('>>> Get engine')
        engine = create_engine(os.environ['DB_CONNECT_STR'])
        return engine

    def get_session(self, engine):
        logger.info('>>> Get session')
        DBSession = sessionmaker(bind=engine)
        self.session = DBSession()
        return self.session

    def create_all(self, engine):
        logger.info('>>> Create all')
        Base.metadata.create_all(engine)
        return

    def add_new_video(self, channel_id, channel_title, video_id, video_title, puplished_at, description):
        logger.info(f'> Add new video: {video_id} = {video_title}')
        new_video = Video(channel_id=channel_id, channel_title=channel_title, video_id=video_id,
                          video_title=video_title, puplished_at=puplished_at, description=description,
                          date_task=datetime.now(), downloading=False, downloaded=False, path_to_video=None)
        self.session.add(new_video)
        self.session.commit()
        return new_video.id

    def get_video_id(self, id):
        logger.info(f">>> Get video id: {id}")
        video = self.session.query(Video).filter(Video.id == id).first()
        return video.video_id

    def check_downloading(self, id):
        logger.info(f">>> Check downloading id: {id}")
        video = self.session.query(Video).filter(Video.id == id).first()
        return video.downloading

    def update_downloading(self, id):
        logger.info(f">>> Update downloading: {id}")
        self.session.query(Video).filter(Video.id == id).update({"downloading": True})
        self.session.commit()
        return

    def update_downloaded(self, id):
        logger.info(f">>> Update downloaded: {id}")
        self.session.query(Video).filter(Video.id == id).update({"downloaded": True})
        self.session.commit()
        return